import pandas as pd
from sklearn.linear_model import LogisticRegression

# Load data
data = pd.read_csv("w3classif.csv", header=None)
X = data.iloc[:, :2]
y = data.iloc[:, 2]

# Fit logistic regression model
model = LogisticRegression(random_state=42)
model.fit(X, y)

# Print model parameters
print(f"Intercept: {model.intercept_[0]}")
print(f"Coefficients: {model.coef_[0]}")

