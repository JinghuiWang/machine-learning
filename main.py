import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LinearRegression

# Define the function f(x)
def f(x):
    return x**3 + 1

if __name__ == '__main__':
    # Set the domain and create a linspace
    domain = np.linspace(-1, 1, 100)

    # Calculate true function values
    true_values = f(domain)

    # Add Gaussian noise to the output/target values
    noise = np.random.normal(0, 0.1, len(domain)) # Adjust the second argument (standard deviation) to control the noise level
    observed_values = true_values + noise

    # Plot the true function
    plt.plot(domain, true_values, label='True Function (f(x) = x^3 + 1)', color='blue')

    # Plot the observed data with Gaussian noise
    plt.scatter(domain, observed_values, label='Observed Data', color='red', s=20)

    # Customize the plot
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.title('True Function and Observed Data with Gaussian Noise')
    plt.legend()
    plt.grid(True)

    # Show the plot
    plt.show()

    # Generate a random set of 30 x/input values in the range [-1, 1]
    x_train = np.random.uniform(-1, 1, 30)

    # Evaluate the true function at each input value and add Gaussian random noise
    y_train = f(x_train) + np.random.normal(0, 0.1, len(x_train))

    # Plot the true function
    plt.plot(domain, true_values, label='True Function (f(x) = x^3 + 1)', color='blue')

    # Plot the observed data with Gaussian noise
    plt.scatter(domain, observed_values, label='Observed Data', color='red', s=20, alpha=0.5)

    # Plot the sample training set
    plt.scatter(x_train, y_train, label='Sample Training Set', color='green', s=30, marker='x')

    # Customize the plot
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.title('True Function, Observed Data with Gaussian Noise, and Sample Training Set')
    plt.legend()
    plt.grid(True)

    # Show the plot
    plt.show()

    # Perform linear regression on the training set
    x_train_reshaped = x_train.reshape(-1, 1)
    ones = np.ones((x_train_reshaped.shape[0], 1))
    X_train = np.hstack((ones, x_train_reshaped))
    coefficients = np.linalg.inv(X_train.T @ X_train) @ X_train.T @ y_train

    # Calculate the linear regression function
    def linear_regression(x):
        return coefficients[0] + coefficients[1] * x


    # Calculate the predicted values on the training set
    y_train_pred = linear_regression(x_train)

    # Calculate the training set sum of squares error
    sum_of_squares_error = np.sum((y_train - y_train_pred) ** 2)

    print("Linear regression coefficients:", coefficients)
    print("Training set sum of squares error:", sum_of_squares_error)

    # Plot the true function, observed data, sample training set, and linear regression line
    plt.plot(domain, true_values, label='True Function (f(x) = x^3 + 1)', color='blue')
    plt.scatter(domain, observed_values, label='Observed Data', color='red', s=20, alpha=0.5)
    plt.scatter(x_train, y_train, label='Sample Training Set', color='green', s=30, marker='x')
    plt.plot(domain, linear_regression(domain), label='Linear Regression', color='orange', linestyle='--')
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.title('True Function, Observed Data with Gaussian Noise, Sample Training Set, and Linear Regression')
    plt.legend()
    plt.grid(True)
    plt.show()

    # Perform polynomial regression for a given degree
    def polynomial_regression(x, y, degree):
        x_reshaped = x.reshape(-1, 1)
        X = np.ones((x_reshaped.shape[0], 1))

        for i in range(1, degree + 1):
            X = np.hstack((X, x_reshaped ** i))

        coefficients = np.linalg.inv(X.T @ X) @ X.T @ y
        return coefficients

    # Calculate the polynomial regression function for a given degree
    def poly_function(x, degree, coefficients):
        result = coefficients[0]
        for i in range(1, degree + 1):
            result += coefficients[i] * x ** i
        return result


    # Experiment with different-order polynomials and observe the training set error
    degrees = [1, 2, 3, 4, 5]
    colors = ['orange', 'purple', 'cyan', 'magenta', 'brown']

    plt.plot(domain, true_values, label='True Function (f(x) = x^3 + 1)', color='blue')
    plt.scatter(domain, observed_values, label='Observed Data', color='red', s=20, alpha=0.5)
    plt.scatter(x_train, y_train, label='Sample Training Set', color='green', s=30, marker='x')

    for i, degree in enumerate(degrees):
        coefficients = polynomial_regression(x_train, y_train, degree)
        y_train_pred = poly_function(x_train, degree, coefficients)
        sum_of_squares_error = np.sum((y_train - y_train_pred) ** 2)

        print(f"Degree {degree} polynomial coefficients:", coefficients)
        print(f"Degree {degree} training set sum of squares error:", sum_of_squares_error)

        plt.plot(domain, poly_function(domain, degree, coefficients), label=f'Degree {degree} Polynomial',
                 color=colors[i], linestyle='--')

    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.title('True Function, Observed Data with Gaussian Noise, Sample Training Set, and Polynomial Regression')
    plt.legend()
    plt.grid(True)
    plt.show()

    print()

    # Read the CSV file
    data = pd.read_csv("pokemonregr.csv")

    # Preview the first few lines of the dataset
    print(data.head())

    # Impute missing values using the mean
    imputer = SimpleImputer(strategy='mean')
    data_imputed = imputer.fit_transform(data)

    # Prepare the input and output/target data
    X = data_imputed[:, :-1]  # All columns except the last one (weight in kg)
    y = data_imputed[:, -1]  # The last column (weight in kg)

    # Fit the linear regression model
    model = LinearRegression()
    model.fit(X, y)

    # Make predictions using the model
    y_pred = model.predict(X)

    # Calculate the sum of squares error
    sum_of_squares_error = np.sum((y - y_pred) ** 2)

    print("Linear regression coefficients:", model.coef_)
    print("Linear regression intercept:", model.intercept_)
    print("Sum of squares error:", sum_of_squares_error)

    # Print coefficients and their corresponding feature names
    coefficients = model.coef_
    feature_names = data.columns[:-1]

    print("Feature importances:")
    for feature, coef in zip(feature_names, coefficients):
        print(f"{feature}: {coef}")

    # Find the most important feature
    most_important_feature_index = coefficients.argmax()
    print(f"\nThe most important feature for predicting weight is: {feature_names[most_important_feature_index]}")