import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler

# Read the CSV file
data = pd.read_csv("pokemonregr.csv")

# Impute missing values using the mean
imputer = SimpleImputer(strategy='mean')
data_imputed = imputer.fit_transform(data)

# Prepare the input and output/target data
X = data_imputed[:, :-1]  # All columns except the last one (weight in kg)
y = data_imputed[:, -1]  # The last column (weight in kg)

# Normalize the input features
scaler = StandardScaler()
X_normalized = scaler.fit_transform(X)

# Fit the linear regression model on normalized features
model_normalized = LinearRegression()
model_normalized.fit(X_normalized, y)

# Print coefficients and their corresponding feature names for the normalized model
coefficients_normalized = model_normalized.coef_
feature_names = data.columns[:-1]

print("Normalized feature importances:")
for feature, coef in zip(feature_names, coefficients_normalized):
    print(f"{feature}: {coef}")

# Find the most important feature
most_important_feature_index_normalized = np.abs(coefficients_normalized).argmax()
print(f"\nThe most important feature for predicting weight with normalized data is: {feature_names[most_important_feature_index_normalized]}")
